{-# LANGUAGE GADTs #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE KindSignatures #-}
{-# LANGUAGE TypeInType #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE RebindableSyntax #-}

module Door where

import Prelude
import Control.Monad.Indexed.State
import Control.Monad.Indexed
import Data.Kind
import Data.Singletons.Prelude.Bool

data DoorState = Open | Closed

data Door :: DoorState -> Type where
  OpenDoor :: Door Open
  ClosedDoor :: Door Closed

open :: IxState (Door Closed) (Door Open) ()
open = iput OpenDoor

close :: IxState (Door Open) (Door Closed) ()
close = iput ClosedDoor

knock :: IxState (Door Closed) (Door Closed) ()
knock = pure ()

-- compiles :: IxState (Door Closed) (Door Closed) ()
compiles = do
  knock
  knock
  open
  close
  knock
  where
    (>>=) = (>>>=) :: IxState i j a -> (a -> IxState j k b) -> IxState i k b
    (>>) a = (>>>=) a . const
    return = ireturn :: a -> IxState i i a

doesntCompile = do
  open
  open
  where
    (>>=) = (>>>=) :: IxState i j a -> (a -> IxState j k b) -> IxState i k b
    (>>) a = (>>>=) a . const
    return = ireturn :: a -> IxState i i a

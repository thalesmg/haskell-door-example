{-# LANGUAGE GADTs #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE KindSignatures #-}
{-# LANGUAGE TypeInType #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE RebindableSyntax #-}

module Door2 where

import Prelude
import Data.Kind

data DoorState = DoorOpen | DoorClosed

data DoorCmd :: Type -> DoorState -> DoorState -> Type where
  Open :: DoorCmd () DoorClosed DoorOpen
  Close :: DoorCmd () DoorOpen DoorClosed
  RingBell :: DoorCmd () DoorClosed DoorClosed
  Pure :: ty -> DoorCmd ty state state
  Bind :: DoorCmd a state1 state2
           -> (a -> DoorCmd b state2 state3)
           -> DoorCmd b state1 state3

compiles = do
  RingBell
  RingBell
  Open
  Close
  RingBell
  where
    (>>=) = Bind
    (>>) a = (>>=) a . const
    return = Pure

doesntCompile = do
  Open
  Open
  where
    (>>=) = Bind
    (>>) a = (>>=) a . const
    return = Pure
